#******************************************************************
#**
#**  camerieri package config master build script
#**
#******************************************************************
#** 
#** Copyright (C) 2012 Snowball Factory Inc.
#** 
#******************************************************************

# we need cmake 2.8.3
cmake_minimum_required(VERSION 2.8.3)

# setup our package info
SET(ANTIPASTI_NAME "camerieri-conf-packages-local")
SET(ANTIPASTI_PROVIDES "camerieri-conf-packages")
SET(ANTIPASTI_REPLACES "camerieri-conf-packages")
SET(ANTIPASTI_CONFLICTS "camerieri-conf-packages")
SET(ANTIPASTI_ARCH_ALL 1)
SET(ANTIPASTI_PREDEPENDS "bash, antipasti, antipasti-conf, camerieri, cron, coreutils")
SET(ANTIPASTI_CONTROL_FILES "postinst")
SET(ANTIPASTI_CONTACT "Awe.sm Packages <packages@awe.sm>")
SET(ANTIPASTI_DESCRIPTION "Camerieri configuration for package repository (local delivery)
 Configuration for camerieri based based package repository (local delivery).")

# setup our files to install
SET(CMAKE_INSTALL_PREFIX "/")
INSTALL(FILES
	packages.conf
	DESTINATION "usr/local/etc/camerieri")
INSTALL(FILES
	cron-packages
	DESTINATION etc/cron.d
	RENAME packages)
INSTALL(FILES
	copyright
	DESTINATION "usr/local/share/doc/${ANTIPASTI_NAME}")

# include antipasti
INCLUDE(Antipasti)

